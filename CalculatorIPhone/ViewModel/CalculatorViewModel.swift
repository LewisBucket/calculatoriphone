//
//  CalculatorViewModel.swift
//  CalculatorIPhone
//
//  Created by Pedro Ferreira on 27/07/21.
//

import SwiftUI
import Foundation

enum OperatorType{
    case none
    case plus
    case subt
    case multi
    case div
    case per
    case result
}
enum OperatorType1{
    case nule
}

class CalculatorViewModel: ObservableObject {
    @State private var taskListVM = TalkListViewModel()
    static let shared = CalculatorViewModel()
    private var firstNumber: String = ""
    private var secondNumber: String = ""
    var operatorCalc: OperatorType = .none
    var operatorCalc1: OperatorType1 = .nule
    var clear: Bool = false
    @Published var screenValue: String = "0"
    @Published var arrayHistoricalResult = [String]()
    @Published var arrayHistoricalAll = [String]()
    @Published var value: String = ""
    @Published var value1: String = ""
    func addNumbers(num: String) {
        if clear {
            screenValue = ""
            clear = false
        }
        
        if screenValue == "0" {
            screenValue = num
        } else {
            screenValue += num
        }
        
        if operatorCalc == .none {
            firstNumber = screenValue
            
        } else {
            secondNumber = screenValue
            
        }
    }
    
    func increase() {
        screenValue = "\(Double(screenValue)! + Double(1))"
    }
    
    func reset() {
        screenValue = "0"
        firstNumber = ""
        secondNumber = ""
        operatorCalc = .none
        operatorCalc1 = .nule
        value = ""
    }
    
    func prints() {
        print("FN: \(firstNumber) -- SN: \(secondNumber)")
    }
    
    func buttonPositiveNegative() {
        screenValue = "\(Double(screenValue)! * Double(-1))"
        firstNumber = "\(Double(firstNumber)! * Double(-1))"
    }
    
    func tappedButtonOperation(operatorCalc: OperatorType){
        clear = true
        
        var finalOperator = operatorCalc
        if(operatorCalc == .result) {
            finalOperator = self.operatorCalc
        }
        
        self.operatorCalc = operatorCalc
        if operatorCalc == .per {
            screenValue = "\(Double(firstNumber)! / Double(100))"
            print(" case subt FN: \(firstNumber) -- SN: \(secondNumber) -- value: \(value)")
        }
        
        if !firstNumber.isEmpty  && !secondNumber.isEmpty {
            switch finalOperator {
            case .plus:
                value = "\(Double(firstNumber)! + Double(secondNumber)!)"
                arrayHistoricalAll.append("\(firstNumber) + \(secondNumber) = \(value)")
                print(" case plus FN: \(firstNumber) -- SN: \(secondNumber) -- value: \(value)")
            case .subt:
                value = "\(Double(firstNumber)! - Double(secondNumber)!)"
                arrayHistoricalAll.append("\(firstNumber) - \(secondNumber) = \(value)")
            case .multi:
                value = "\(Double(firstNumber)! * Double(secondNumber)!)"
                arrayHistoricalAll.append("\(firstNumber) * \(secondNumber) = \(value)")
            case .div:
                value = "\(Double(firstNumber)! / Double(secondNumber)!)"
                arrayHistoricalAll.append("\(firstNumber) / \(secondNumber) = \(value)")
                
            default:
                
                break
            }
            
            firstNumber = value
            secondNumber = ""
            self.screenValue = value
            
            arrayHistoricalResult.append(screenValue)
            print(arrayHistoricalResult)
            print(arrayHistoricalAll)
            
        }
    }
}
