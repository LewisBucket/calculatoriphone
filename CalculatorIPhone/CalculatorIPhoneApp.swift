//
//  CalculatorIPhoneApp.swift
//  CalculatorIPhone
//
//  Created by Pedro Ferreira on 21/07/21.
//  Optimizada para IPhone 8.

import SwiftUI

@main
struct CalculatorIPhoneApp: App {
    var body: some Scene {
        WindowGroup {
            CalculatorView()
        }
    }
}
