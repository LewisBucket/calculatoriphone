//
//  CalculatorModel.swift
//  CalculatorIPhone
//
//  Created by Pedro Ferreira on 21/07/21.
//

import Foundation
import SwiftUI

struct ButtonCal: View {
    var text: String
    var color: Color
    var colorText: Color
    var action: () -> ()
    var body: some View {
        Button(action: {
            action()
        }, label: {
            Circle()
                .fill(color)
                .frame(width: 80, height: 80)
        })
        .overlay(Text(text)
                    .font(.system(size: 35))
                    //  .fontWeight(.bold)
                    .foregroundColor(colorText))
        .buttonStyle(PlainButtonStyle())
    }
}
