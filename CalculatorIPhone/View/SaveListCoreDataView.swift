//
//  SaveListCoreDataView.swift
//  CalculatorIPhone
//
//  Created by Pedro Ferreira on 27/07/21.
//  Optimizada para IPhone 8.

import SwiftUI

struct CoreDataView: View {
    @StateObject private var taskListVM = TalkListViewModel()
    @StateObject var calculatorVM = CalculatorViewModel()
    var body: some View {
        
        VStack{
            Text("Historial CoreData")
            List(taskListVM.tasks, id: \.id) { task in
                Text(task.title)
            }
            .onAppear{taskListVM.getAllTasks()}
            Spacer()
            ListHistoricalView()
        }.padding()
    }
}

struct TaskListCoreDataView_Previews: PreviewProvider {
    static var previews: some View {
        CoreDataView()
    }
}

