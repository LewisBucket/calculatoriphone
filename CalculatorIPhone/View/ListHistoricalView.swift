//
//  ListHistoricalView.swift
//  CalculatorIPhone
//
//  Created by Pedro Ferreira on 22/07/21.
//  Optimizada para IPhone 8.

import SwiftUI
import Foundation

struct ListHistoricalView: View {
    @State var arrayHistorical2 = [String]()
    @ObservedObject var calculatorVM = CalculatorViewModel.shared
    
    var body: some View {
        ZStack{
            VStack{
                HStack{
                    Text("Historico")
                        .font(.title)
                        .padding(.trailing, 220)
                }
                Spacer()
                List {
                    ForEach(0..<self.calculatorVM.arrayHistoricalAll.count) { historial in
                        Text("\(self.calculatorVM.arrayHistoricalAll[historial])")
                            .foregroundColor(.gray)
                        
                        Text("\(self.calculatorVM.arrayHistoricalResult[historial])")
                            .foregroundColor(.black)
                    }
                }
            }
        }
    }
    
    struct ListHistorical_Previews: PreviewProvider {
        static var previews: some View {
            ListHistoricalView()
        }
    }
}
