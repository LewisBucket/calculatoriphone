//
//  CalculatorView.swift
//  CalculatorIPhone
//
//  Created by Pedro Ferreira on 27/07/21.
//  Optimizada para IPhone 8.

import SwiftUI
import UIKit

struct CalculatorView: View {
    @State private var showModalView = false
    @ObservedObject private var calculatorVM = CalculatorViewModel.shared
    @ObservedObject private var taskListVM = TalkListViewModel()
    
    var body: some View {
        NavigationView {
            ZStack {
                Color.black.ignoresSafeArea()
                VStack(spacing: 5) {
                    Spacer()
                    HStack {
                        Spacer()
                        Text("\(calculatorVM.screenValue)")
                            .font(.system(size: 80))
                            .foregroundColor(.white)
                            .multilineTextAlignment(.center)
                            .navigationBarItems(trailing: Button {
                                showModalView = true
                            } label: {
                                Image(systemName: "clock.arrow.circlepath").font(.title2).accentColor(.gray)
                                
                            })
                            .sheet(isPresented: $showModalView) {
                                
                                CoreDataView()
                            }
                    }
                    .padding(.horizontal, 25)
                    
                    HStack {
                        ButtonCal(text: "AC",
                                  color: Color.gray,
                                  colorText: Color.white, action: {
                                    calculatorVM.reset()
                                  })
                        
                        ButtonCal(text: "+/-",
                                  color: Color.gray,
                                  colorText: Color.white, action: {
                                    calculatorVM.buttonPositiveNegative()
                                    calculatorVM.prints()
                                  })
                        
                        ButtonCal(text: "%",
                                  color: Color.gray,
                                  colorText: Color.white, action: {
                                    calculatorVM.tappedButtonOperation(operatorCalc: .per)
                                  })
                        
                        ButtonCal(text: "÷",
                                  color: Color.orange,
                                  colorText: Color.white, action: {
                                    
                                    calculatorVM.tappedButtonOperation(operatorCalc: .div)
                                  })
                        
                    }
                    
                    HStack{
                        
                        ButtonCal(text: "7",
                                  color: Color("Gray2"),
                                  colorText: Color.white, action: {
                                    calculatorVM.addNumbers(num: "7")
                                    
                                  })
                        ButtonCal(text: "8",
                                  color: Color("Gray2"),
                                  colorText: Color.white, action: {
                                    calculatorVM.addNumbers(num: "8")
                                    
                                  })
                        ButtonCal(text: "9",
                                  color: Color("Gray2"),
                                  colorText: Color.white, action: {
                                    calculatorVM.addNumbers(num: "9")
                                    
                                  })
                        ButtonCal(text: "x",
                                  color: Color.orange,
                                  colorText: Color.white, action: {
                                    calculatorVM.tappedButtonOperation(operatorCalc: .multi)
                                    
                                  })
                        
                    }
                    HStack {
                        ButtonCal(text: "4",
                                  color: Color("Gray2"),
                                  colorText: Color.white, action: {
                                    calculatorVM.addNumbers(num: "4")
                                    
                                  })
                        ButtonCal(text: "5",
                                  color: Color("Gray2"),
                                  colorText: Color.white, action: {
                                    calculatorVM.addNumbers(num: "5")
                                    
                                  })
                        ButtonCal(text: "6",
                                  color: Color("Gray2"),
                                  colorText: Color.white, action: {
                                    calculatorVM.addNumbers(num: "6")
                                    
                                  })
                        ButtonCal(text: "-",
                                  color: Color.orange,
                                  colorText: Color.white, action: {
                                    calculatorVM.tappedButtonOperation(operatorCalc: .subt)
                                    
                                  })
                        
                        
                    }
                    HStack{
                        ButtonCal(text: "1",
                                  color: Color("Gray2"),
                                  colorText: Color.white, action: {
                                    calculatorVM.addNumbers(num: "1")
                                    // calculatorVM.createCalcDigit("1")
                                    
                                  })
                        ButtonCal(text: "2",
                                  color: Color("Gray2"),
                                  colorText: Color.white, action: {
                                    calculatorVM.addNumbers(num: "2")
                                    // calculatorVM.num2()
                                    
                                  })
                        ButtonCal(text: "3",
                                  color: Color("Gray2"),
                                  colorText: Color.white, action: {
                                    calculatorVM.addNumbers(num: "3")
                                    
                                  })
                        ButtonCal(text: "+",
                                  color: Color.orange,
                                  colorText: Color.white, action: {
                                    calculatorVM.tappedButtonOperation(operatorCalc: .plus)
                                    
                                  })
                    }
                    
                    HStack {
                        Button(action: {
                            calculatorVM.addNumbers(num: "0")
                        }, label: {
                            HStack {
                                Text("0")
                                    .font(.system(size: 45))
                                    .foregroundColor(.white)
                                    .padding(.leading)
                                Spacer()
                            }
                            .padding(10)
                            .background(Color("Gray2"))
                            .cornerRadius(50)
                        })
                        
                        ButtonCal(text: ".",
                                  color: Color("Gray2"),
                                  colorText: Color.white, action: {
                                    calculatorVM.addNumbers(num: ".")
                                  })
                        
                        ButtonCal(text: "=",
                                  color: Color.orange,
                                  colorText: Color.white, action: {
                                    calculatorVM.tappedButtonOperation(operatorCalc: .result)
                                    taskListVM.title = calculatorVM.screenValue
                                    print("Save CoreData:\(calculatorVM.screenValue)")
                                    taskListVM.save()
                                    
                                    
                                  })
                    }.padding(.horizontal)
                }
            }
        }
    }
    
    struct ButtonCal: View {
        var text: String
        var color: Color
        var colorText: Color
        var action: () -> ()
        var body: some View {
            Button(action: {
                action()
            }, label: {
                Circle()
                    .fill(color)
                    .frame(width: 80, height: 80)
            })
            .overlay(Text(text)
                        .font(.system(size: 35))
                        //  .fontWeight(.bold)
                        .foregroundColor(colorText))
            .buttonStyle(PlainButtonStyle())
        }
    }
    
    struct ContentView_Previews: PreviewProvider {
        static var previews: some View {
            CalculatorView()
        }
    }
    
}
