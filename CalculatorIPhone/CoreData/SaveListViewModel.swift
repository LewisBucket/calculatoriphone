//
//  SaveListViewModel.swift
//  CalculatorIPhone
//
//  Created by Pedro Ferreira on 27/07/21.
//

import Foundation

class TalkListViewModel: ObservableObject {
    
    @Published var title: String = ""
    @Published var tasks: [TaskViewModel] = []
    
    func getAllTasks(){
        tasks =  CoreDataManager.shared.getAllTasks().map(TaskViewModel.init)
    }
    
    func save() {
        let task = Task(context: CoreDataManager.shared.persistentContainer.viewContext)
        task.title = title
        CoreDataManager.shared.save()
    }
}

struct TaskViewModel {
    let task: Task
    var id: NSObject {
        return task.objectID
    }
    var title: String {
        return task.title ?? ""
    }
}

